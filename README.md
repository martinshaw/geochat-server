# geochat-server
## Geolocation-based messaging and media content sharing platform

For the accompanying client application: https://gitlab.com/martinshaw/geochat-client

*Copyright (C) Martin David Shaw - All Rights Reserved.*
*Unauthorized copying of this file, via any medium is strictly prohibited, proprietary and confidential.*
*Written by Martin David Shaw <thirdyearproject@martinshaw.co> <15090190@stu.mmu.ac.uk>, November 2017.*